######################################################################
# This is for translation ONLY, use build.sh for building
######################################################################

TEMPLATE = aux
TARGET = install-history
CONFIG += sailfishapp sailfishapp_i18n

lupdate_only {
    SOURCES += qml/pages/MainPage.qml \
               qml/pages/StatsPage.qml
}

# Input
TRANSLATIONS += translations/settings-install-history-de.ts \
                translations/settings-install-history-en.ts \
                translations/settings-install-history-ru.ts \
                translations/settings-install-history-sv.ts
