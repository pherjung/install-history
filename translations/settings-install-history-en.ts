<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>MainPage</name>
    <message numerus="yes">
        <source>%Ln event(s)</source>
        <comment>very, very unlikely to have only one, still, plurals please!</comment>
        <translation>
            <numerusform>%Ln event</numerusform>
            <numerusform>%Ln events</numerusform>
        </translation>
    </message>
    <message>
        <source>Package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>n/a</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Verbose Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reduced Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search by Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search by Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Install History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>local</source>
        <comment>name for a local repo</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>local</source>
        <comment>type of a local repo</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Executes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StatsPage</name>
    <message numerus="yes">
        <source>Installation(s): %Ln</source>
        <comment>very, very unlikely to have one or less, still, plurals please!</comment>
        <translation type="obsolete">
            <numerusform>Installation: %Ln</numerusform>
            <numerusform>Installations: %Ln</numerusform>
        </translation>
    </message>
    <message>
        <source>%L1</source>
        <comment>number of events</comment>
        <translation>%L1</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation>(%L1%)</translation>
    </message>
    <message>
        <source>View repositories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Crunching Numbers…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Install History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>others</source>
        <comment>things that don&apos;t fit in a category</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Most active packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Most active Repositories</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
