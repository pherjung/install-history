<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>MainPage</name>
    <message>
        <source>Install History</source>
        <translation>Installationsverlauf</translation>
    </message>
    <message>
        <source>%L1 records</source>
        <translation type="vanished">%L1 Einträge</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>hide search</source>
        <translation type="vanished">Suche ausblenden</translation>
    </message>
    <message>
        <source>Search on %1</source>
        <translation>Auf %1 suchen</translation>
    </message>
    <message>
        <source>Jump to…</source>
        <translation type="vanished">Springe zu…</translation>
    </message>
    <message>
        <source>tap to select</source>
        <translation type="vanished">Tippe zum Ändern</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>Installiert</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation>Entfernt</translation>
    </message>
    <message>
        <source>Repository</source>
        <translation>Quelle</translation>
    </message>
    <message>
        <source>n/a</source>
        <translation>n.n.</translation>
    </message>
    <message>
        <source>Package Name</source>
        <translation type="vanished">Paketname</translation>
    </message>
    <message>
        <source>Hide search</source>
        <translation>Suche verbergen</translation>
    </message>
    <message>
        <source>Search by Date</source>
        <translation>Suche nach Datum</translation>
    </message>
    <message>
        <source>Search by Name</source>
        <translation>Suche nach Name</translation>
    </message>
    <message>
        <source>Package</source>
        <translation>Paketname</translation>
    </message>
    <message>
        <source>Verbose Display</source>
        <translation>Auführliche Ansicht</translation>
    </message>
    <message>
        <source>Reduced Display</source>
        <translation>Reduzierte Ansicht</translation>
    </message>
    <message>
        <source>local</source>
        <comment>name for a local repo</comment>
        <translation>lokal</translation>
    </message>
    <message>
        <source>local</source>
        <comment>type of a local repo</comment>
        <translation>lokal</translation>
    </message>
    <message>
        <source>%L1 events</source>
        <translation type="vanished">%L1 Ereignisse</translation>
    </message>
    <message numerus="yes">
        <source>%Ln event(s)</source>
        <comment>very, very unlikely to have only one, still, plurals please!</comment>
        <translation>
            <numerusform>%Ln Ereignis</numerusform>
            <numerusform>%Ln Ereignisse</numerusform>
        </translation>
    </message>
    <message>
        <source>Application</source>
        <translation>App</translation>
    </message>
    <message>
        <source>Executes</source>
        <translation>Startet</translation>
    </message>
</context>
<context>
    <name>StatsPage</name>
    <message>
        <source>Install History</source>
        <translation>Installationsverlauf</translation>
    </message>
    <message>
        <source>Statistics</source>
        <translation>Statistiken</translation>
    </message>
    <message>
        <source>Installations: %L1</source>
        <translation type="vanished">Installationen: %L1</translation>
    </message>
    <message>
        <source>(%L1%)</source>
        <translation type="vanished">(L1%)</translation>
    </message>
    <message>
        <source>(%L1%)</source>
        <comment>percentage in parentheses</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message numerus="yes">
        <source>Installation(s): %Ln</source>
        <comment>very, very unlikely to have one or less, still, plurals please!</comment>
        <translation type="vanished">
            <numerusform>Installation</numerusform>
            <numerusform>Installationen</numerusform>
        </translation>
    </message>
    <message>
        <source>View repositories</source>
        <translation>Quellen anzeigen</translation>
    </message>
    <message>
        <source>Active packages</source>
        <translation type="vanished">Paketaktivitäten</translation>
    </message>
    <message>
        <source>View packages</source>
        <translation>Pakete anzeigen</translation>
    </message>
    <message>
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation>Prozentangaben relativ zu allen Installationsereignissen. Balken relativ zum Element mit der höchsten Anzahl an Ereignissen.</translation>
    </message>
    <message>
        <source>Repositories</source>
        <translation type="vanished">Quellen</translation>
    </message>
    <message>
        <source>others</source>
        <comment>things that don&apos;t fit in a category</comment>
        <translation>sonstige</translation>
    </message>
    <message>
        <source>%L1</source>
        <comment>number of events</comment>
        <translation>%L1</translation>
    </message>
    <message>
        <source>Crunching Numbers…</source>
        <translation>Berechne…</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation>(%L1%)</translation>
    </message>
    <message>
        <source>Most active packages</source>
        <translation>Meistgeänderte Pakete</translation>
    </message>
    <message>
        <source>Most active Repositories</source>
        <translation>Meistverwendete Quellen</translation>
    </message>
</context>
</TS>
