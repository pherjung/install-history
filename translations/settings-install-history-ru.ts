<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>MainPage</name>
    <message>
        <source>Package</source>
        <translation>Пакет</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>Установлен</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation>Удалён</translation>
    </message>
    <message>
        <source>Repository</source>
        <translation>Репозиторий</translation>
    </message>
    <message>
        <source>n/a</source>
        <translation>неизвестно</translation>
    </message>
    <message>
        <source>Hide search</source>
        <translation>Скрыть поиск</translation>
    </message>
    <message>
        <source>Search by Date</source>
        <translation>Искать по дате</translation>
    </message>
    <message>
        <source>Search by Name</source>
        <translation>Искать по названию</translation>
    </message>
    <message>
        <source>Install History</source>
        <translation>История установки пакетов</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <source>Search on %1</source>
        <translation>Поиск в %1</translation>
    </message>
    <message numerus="yes">
        <source>%Ln event(s)</source>
        <comment>very, very unlikely to have only one, still, plurals please!</comment>
        <translation>
            <numerusform>%Ln действие</numerusform>
            <numerusform>%Ln действия</numerusform>
            <numerusform>%Ln действий</numerusform>
        </translation>
    </message>
    <message>
        <source>Verbose Display</source>
        <translation>Обычный список</translation>
    </message>
    <message>
        <source>Reduced Display</source>
        <translation>Компактный список</translation>
    </message>
    <message>
        <source>local</source>
        <comment>name for a local repo</comment>
        <translation>local</translation>
    </message>
    <message>
        <source>local</source>
        <comment>type of a local repo</comment>
        <translation>локальный</translation>
    </message>
    <message>
        <source>Application</source>
        <translation>Приложение</translation>
    </message>
    <message>
        <source>Executes</source>
        <translation>Команда запуска</translation>
    </message>
</context>
<context>
    <name>StatsPage</name>
    <message>
        <source>Install History</source>
        <translation>История установки пакетов</translation>
    </message>
    <message>
        <source>Statistics</source>
        <translation>Статистика</translation>
    </message>
    <message numerus="yes">
        <source>Installation(s): %Ln</source>
        <comment>very, very unlikely to have one or less, still, plurals please!</comment>
        <translation type="obsolete">
            <numerusform>Установлен: %Ln</numerusform>
            <numerusform>Установлено: %Ln</numerusform>
            <numerusform>Установлено: %Ln</numerusform>
        </translation>
    </message>
    <message>
        <source>View repositories</source>
        <translation>Просмотр репозиториев</translation>
    </message>
    <message>
        <source>Active packages</source>
        <translation type="vanished">Активные пакеты</translation>
    </message>
    <message>
        <source>View packages</source>
        <translation>Просмотр пакетов</translation>
    </message>
    <message>
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation>Процент считается от числа всех действий по установке пакетов. Полоски соотносятся с данными о пакете, с которым совершено наибольшее число действий.</translation>
    </message>
    <message>
        <source>Repositories</source>
        <translation type="vanished">Репозитории</translation>
    </message>
    <message>
        <source>others</source>
        <comment>things that don&apos;t fit in a category</comment>
        <translation>прочее</translation>
    </message>
    <message>
        <source>%L1</source>
        <comment>number of events</comment>
        <translation>%L1</translation>
    </message>
    <message>
        <source>Crunching Numbers…</source>
        <translation>Идёт подсчет…</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation type="vanished">(%L1%)</translation>
    </message>
    <message>
        <source>(%L1%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation>(%L1%)</translation>
    </message>
    <message>
        <source>Most active packages</source>
        <translation>Самые активные пакеты</translation>
    </message>
    <message>
        <source>Most active Repositories</source>
        <translation>Самые активные репозитории</translation>
    </message>
</context>
</TS>
