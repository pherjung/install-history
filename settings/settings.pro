######################################################################
# This is for translation ONLY, use build.sh for building
######################################################################

TEMPLATE = aux
TARGET = install-history
CONFIG += sailfishapp sailfishapp_i18n

lupdate_only {
    SOURCES += entries/translation.qml
}

# Input
TRANSLATIONS += translations/settings-install-history-entry-de.ts \
                translations/settings-install-history-entry-en.ts \
                translations/settings-install-history-entry-ru.ts \
                translations/settings-install-history-entry-sv.ts
